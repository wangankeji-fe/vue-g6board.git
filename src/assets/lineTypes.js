const lineTypes = [{
  label: '电口',
  value: 'electrical',
}, {
  label: '光口',
  value: 'optical',
}, {
  label: '虚拟网络',
  value: 'virtual'
}]

export default lineTypes
