const deviceTypes = [
  {
    label: '终端',
    value: 'pc',
    size: 20,
  },
  {
    label: '交换机',
    value: 'switch',
    size: 30,
  },
  {
    label: '防火墙',
    value: 'firewall',
    size: 40,
  },
  {
    label: '服务器',
    value: 'server',
    size: 40,
  },
]

export default deviceTypes
